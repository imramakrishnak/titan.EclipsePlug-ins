/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.DocumentComment;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.editors.BaseTextHover;
import org.eclipse.titan.designer.editors.IReferenceParser;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReferenceAnalyzer;
import org.eclipse.ui.IEditorPart;

/**
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public final class TextHover extends BaseTextHover {
	private final ISourceViewer sourceViewer;
	private final IEditorPart editor;

	public TextHover(final ISourceViewer sourceViewer, final IEditorPart editor) {
		this.sourceViewer = sourceViewer;
		this.editor = editor;
	}

	@Override
	protected ISourceViewer getSourceViewer() {
		return sourceViewer;
	}

	@Override
	protected IEditorPart getTargetEditor() {
		return editor;
	}

	@Override
	protected IReferenceParser getReferenceParser() {
		return new TTCN3ReferenceParser(false);
	}
	
	/**
	 * Inactive preliminary code for ttcn3 editor hover support
	 *  
	 * @param textViewer
	 * @param hoverRegion
	 * @return
	 */
	// @Override
	public String ___getHoverInfo(final ITextViewer textViewer, final IRegion hoverRegion) {
		IDocument doc = textViewer.getDocument();
		final IFile file = editor.getEditorInput().getAdapter(IFile.class);
		
		final String id = getIdentifier(doc, hoverRegion.getOffset());
		
		Reference ref = TTCN3ReferenceAnalyzer.parseForCompletion(file, id);
		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		final Module tempModule = projectSourceParser.containedModule(file);
		final CompilationTimeStamp timestamp = tempModule.getLastCompilationTimeStamp();
		Scope scope = null;
		
		if (tempModule != null) {
			scope = tempModule.getSmallestEnclosingScope(hoverRegion.getOffset());
		}
		if (ref != null) {
			Assignment assignment = scope.getAssBySRef(timestamp, ref);
			if (assignment instanceof Definition) {
				DocumentComment dc = ((Definition) assignment).getDocumentComment();
				if (dc == null) {
					return "";
				}
				StringBuilder tooltip = new StringBuilder();
				tooltip.append("<b>Authors:</b><br>");
				for (String author : dc.getAuthors()) {
					tooltip.append(author + "<br>");
				}
				
				return tooltip.toString();
			}
		}
		return "";
	}
	
	private String getIdentifier(IDocument doc, int pos) {
		try {
            char c = doc.getChar(pos);
            if (isIdChar(c) == false) {
            	return "";
            }

            int start = 0;
            for (int pt = pos; pt >= 0 && isIdChar(doc.getChar(pt)) ; pt--) {
            	start = pt;
            }

            int end = 0;
            for (int pt = pos; pt < doc.getLength() && isIdChar(doc.getChar(pt)) ; pt++) {
            	end = pt;
            }
            String s = doc.get(start, end - start + 1);
            return s;
		} catch (Exception e) {
			
		}
		return null;
	}
	
	private boolean isIdChar(char c) {
		return Character.isAlphabetic(c) || Character.isDigit(c) || c == '_';
	}
}
