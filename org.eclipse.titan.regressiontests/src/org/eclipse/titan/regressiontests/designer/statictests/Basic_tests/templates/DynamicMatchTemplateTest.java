/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.regressiontests.designer.statictests.Basic_tests.templates;

import java.util.ArrayList;

import org.eclipse.core.resources.IMarker;
import org.eclipse.titan.regressiontests.designer.Designer_plugin_tests;
import org.eclipse.titan.regressiontests.library.MarkerToCheck;
import org.junit.Test;

public class DynamicMatchTemplateTest {
	//TempDynamic_SE_ttcn
	@Test
	public void TempImplication_SE_ttcn() throws Exception {
		Designer_plugin_tests.checkSemanticMarkersOnFile(TempDynamic_SE_ttcn_initializer(), "src/Basic_tests/templates/TempDynamic_SE.ttcn");
	}

	private ArrayList<MarkerToCheck> TempDynamic_SE_ttcn_initializer() {
		//TempDynamic_SE.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(8);
		int lineNum = 17;
		markersToCheck.add(new MarkerToCheck("Reference to `value` is not allowed here",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 3;
		markersToCheck.add(new MarkerToCheck("The dynamic template's statement block does not have a return statement",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("Missing return value. The dynamic template's statement block should return a boolean value",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("boolean value was expected",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("A specific value without matching symbols was expected as return value",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("Control might leave the dynamic template's statement block without reaching a return statement",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 18;
		markersToCheck.add(new MarkerToCheck("Restriction 'value' or 'omit' on template does not allow usage of `dynamic match'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("Restriction 'value' or 'omit' on template does not allow usage of `dynamic match'",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
}
