/*****************************************************************
 ** @author  STF 572
 ** @version 0.0.1
 ** @purpose 5.1.1.5, Ensure that use class constructor with its superclass constructor. 
 ** @verdict pass accept
 **
 ** modified by Miklos Magyari
*****************************************************************/
module Sem_5010105_Constructors_002 "TTCN-3:2018 Object-Oriented" {
    
    type component GeneralComp {
    }

    type class MySuperClass {
        private var octetstring v_o;

        create(octetstring pl_o) {
            this.v_o := pl_o;
        }

        public function get_vo() return octetstring {
            return this.v_o;
        }
    }

    type class MySubClass extends MySuperClass {
        private var template charstring vt_cstr;

        create(template charstring pl_cstr) : MySuperClass('AA5600'O) {
            this.vt_cstr := pl_cstr;
        }
    }

    testcase tc_Sem_5010105_Constructors_002() runs on GeneralComp {
        var MySubClass vl_a := MySubClass.create( pattern "?abc*");
        if (vl_a.get_vo() == 'AA5600'O) {
            setverdict(pass);
        } else {
            setverdict(fail);
        }
    }

    control {
        execute(tc_Sem_5010105_Constructors_002());
    }
}